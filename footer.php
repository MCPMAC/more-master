	<!--BEGIN: Footer Section-->
	<footer class="footer clear-fix row-site">

		<!--BEGIN: Footer Nav-->
		<nav role="navigation">
			<h1 class="access-hide">Footer Navigation</h1>
			<ul class="horiz-list">
				<?php wp_nav_menu('menu=footerNav'); ?>
			</ul>
		</nav>
		<!--END: Footer Nav-->

		<!--BEGIN: Optional Contact Info using microformats: http://microformats.org/-->
		<!--dl class="vcard">
			<dt class="org fn">OrgName or Full Name of person - remove one or the other class</dt>
			<dd class="adr">
				<span class="street-address"></span>
				<span class="locality">City</span>
				<span class="region">State</span>
				<span class="postal-code">xxxxx</span>		
			</dd>
			<dd class="tel"></dd>
			<dd class="tel"></dd>
			<dd class="email"><a href="mailto:"></a></dd>
			<dd class="fax"></dd>
		</dl-->
		<!--END: Contact Info-->
		
		<!--BEGIN: Credits :: If you use this theme please consider keeping our credit in the footer.  You can delete it if you need to. -->
		<small> Site By: <a class="credit" href="http://alittlemorelikethis.com" title="More And Co.">More &amp; Co.</a></small>
		<!--END: Credits-->
		
		<p class="copyright"><small>&copy; <?php echo date('Y'); ?> <?php bloginfo('name')?></small></p>
		
		<!-- wp_footer hook for Plugins -->
		<?php wp_footer(); ?>

	</footer>
	<!--END: Footer Section-->

</body>
</html>