<?php


// Removes tags generated in the WordPress Head that we don't use, you could read up and re-enable them if you think they're needed
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'rsd_link'); // Might be necessary if you or other people on this site use remote editors.
remove_action('wp_head', 'feed_links_extra', 3); 
remove_action('wp_head', 'wlwmanifest_link'); // Might be necessary if you or other people on this site use Windows Live Writer.
remove_action('wp_head', 'start_post_rel_link', 10, 0); 
remove_action('wp_head', 'parent_post_rel_link', 10, 0); 
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0); 


/* Hide the version of WordPress you're running from source and RSS feed /*/

function hcwp_remove_version() {return '';}
add_filter('the_generator', 'hcwp_remove_version');


function customize_meta_boxes() {
   /* These remove meta boxes from POSTS */
  remove_post_type_support("post","excerpt"); //Remove Excerpt Support
  //remove_post_type_support("post","author"); //Remove Author Support
  //remove_post_type_support("post","revisions"); //Remove Revision Support
  //remove_post_type_support("post","comments"); //Remove Comments Support
  remove_post_type_support("post","trackbacks"); //Remove trackbacks Support
  //remove_post_type_support("post","editor"); //Remove Editor Support
  remove_post_type_support("post","custom-fields"); //Remove custom-fields Support
  //remove_post_type_support("post","title"); //Remove Title Support

  
  /* These remove meta boxes from PAGES */
  //remove_post_type_support("page","revisions"); //Remove Revision Support
  //remove_post_type_support("page","comments"); //Remove Comments Support
  //remove_post_type_support("page","author"); //Remove Author Support
  remove_post_type_support("page","trackbacks"); //Remove trackbacks Support
  remove_post_type_support("page","custom-fields"); //Remove custom-fields Support
  
}
add_action('admin_init','customize_meta_boxes');

/*
 * Remove senseless dashboard widgets for non-admins. (Un)Comment or delete as you wish.
 */
function remove_dashboard_widgets() {
    global $wp_meta_boxes;

    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']); // Plugins widget
    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']); // WordPress Blog widget
    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']); // Other WordPress News widget
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']); // Right Now widget
    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']); // Quick Press widget
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']); // Incoming Links widget
    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_recent_drafts']); // Recent Drafts widget
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']); // Recent Comments widget
       unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_glance_items']); // Recent Comments widget
}

function themename_configure_dashboard_menu() {
    global $menu,$submenu;

    global $current_user;
    get_currentuserinfo();

        // $menu and $submenu will return all menu and submenu list in admin panel
        
        // $menu[2] = ""; // Dashboard
        // $menu[5] = ""; // Posts
        $menu[15] = ""; // Links
        $menu[25] = ""; // Comments
        // $menu[65] = ""; // Plugins

        // unset($submenu['themes.php'][5]); // Themes
        // unset($submenu['themes.php'][12]); // Editor
}

add_action('wp_dashboard_setup', 'remove_dashboard_widgets'); // Add action to hide dashboard widgets
add_action('admin_head', 'themename_configure_dashboard_menu'); // Add action to hide admin menu items











?>