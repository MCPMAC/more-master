<?php

/*
I ADMIN CUSTOMIZATIONS
II IMAGES
III SCRIPTS
IV THEME SUPPORTS / CLASSES ETC
V REGISTER SIDEBARS
VI POST FORMAT / CUSTOM POST TYPE
VII ANALYTICS / MISC 

*/







/* --------------------------------------------------
 I ADMIN CUSTOMIZATIONS
 ________________________________________________________________________________________________*/






function my_login_logo() { ?>
    <style type="text/css">
        body.login div#login h1 a {
            background-image: url(<?php echo get_bloginfo( 'template_directory' ) ?>/images/site-login.png);
            padding-bottom: 30px;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );


 //Replace the default welcome 'Howdy' in the admin bar with something more professional.
function admin_bar_replace_howdy($wp_admin_bar) {
    $account = $wp_admin_bar->get_node('my-account');
    $replace = str_replace('Howdy,', 'Welcome,', $account->title);            
    $wp_admin_bar->add_node(array('id' => 'my-account', 'title' => $replace));
}
add_filter('admin_bar_menu', 'admin_bar_replace_howdy', 25);

// Add a widget in WordPress Dashboard
function wpc_dashboard_widget_function() {
    // Entering the text between the quotes
    echo "<ul>
    <li>mac@alittlemorelikethis.com with problems</li>
    </ul>";
}
function wpc_add_dashboard_widgets() {
    wp_add_dashboard_widget('wp_dashboard_widget', 'Contact', 'wpc_dashboard_widget_function');
}
add_action('wp_dashboard_setup', 'wpc_add_dashboard_widgets' );



/* REMOVES !*/
require_once('includes/remove.php');




/* --------------------------------------------------
 II IMAGES
 ________________________________________________________________________________________________*/


@ini_set( 'upload_max_size' , '64M' );
@ini_set( 'post_max_size', '64M');
@ini_set( 'max_execution_time', '300' );






// For Responsive images and thumbnails, removes the width and height from the markup
add_filter( 'post_thumbnail_html', 'remove_width_attribute', 10 );
add_filter( 'image_send_to_editor', 'remove_width_attribute', 10 );

function remove_width_attribute( $html ) {
   $html = preg_replace( '/(width|height)="\d*"\s/', "", $html );
   return $html;
}


// Links Featured Images to Permalink
add_filter( 'post_thumbnail_html', 'my_post_image_html', 10, 3 );

function my_post_image_html( $html, $post_id, $post_image_id ) {

$html = '<a href="' . get_permalink( $post_id ) . '" title="' . esc_attr( get_post_field( 'post_title', $post_id ) ) . '">' . $html . '</a>';
return $html;

}



function filter_ptags_on_images($content) {
return preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
}

add_filter('the_content', 'filter_ptags_on_images');


//First Image is post thumbnail*

function catch_that_image() {
  global $post, $posts;
  $first_img = '';
  ob_start();
  ob_end_clean();
  $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
  $first_img = $matches [1] [0];

  if(empty($first_img)){ //Defines a default image
    $first_img = "/images/default.jpg";
  }
  return $first_img;
}


//Royal Slider Skin

add_filter('new_royalslider_skins', 'new_royalslider_add_custom_skin', 10, 2);
function new_royalslider_add_custom_skin($skins) {
$skins['MORE'] = array( 
'label' => 'MORE',
'path' => get_template_directory_uri() . '/slider/rs.css'  
);
return $skins;
}




/* --------------------------------------------------
 III SCRIPTS 
 ________________________________________________________________________________________________*/


// Load jQuery
// you can either load the local or google CDN version below by commenting out one or another wp_register_script line function


    function my_init_method() {
        if (!is_admin()) {

            wp_deregister_script( 'jquery' );

            // local copy of jquery
            //wp_register_script( 'jquery', '/wp-includes/js/jquery/jquery.js"');

            // google CDN copy of jquery
            wp_register_script( 'jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js');
            
            wp_enqueue_script( 'jquery' );
        }
    } 
    add_action('init', 'my_init_method');





/* --------------------------------------------------
 IV THEME SUPPORTS / CLASSES ETC
 ________________________________________________________________________________________________*/




// Add theme support
    
if (function_exists('add_theme_support')) {
    
    // Activates menu features
    add_theme_support('menus');
    
    // Activates Featured Image functions
    add_theme_support( 'post-thumbnails' );

}


function register_my_menus() {
register_nav_menus(
array(
'menu-a' => __( 'Menu A' ),
'menu-b' => __( 'Menu B' ),
'menu-c' => __( 'Menu C' )
)
);
}

add_action( 'init', 'register_my_menus' );




// Add to the body_class function
function condensed_body_class($classes) {
    global $post;

    // add a class for the name of the page - later might want to remove the auto generated pageid class which isn't very useful
    if( is_page()) {
        $pn = $post->post_name;
        $classes[] = "page_".$pn;
    }

    // add a class for the parent page name
    $post_parent = get_post($post->post_parent);
    $parentSlug = $post_parent->post_name;
    
    if ( is_page() && $post->post_parent ) {
            $classes[] = "parent_".$parentSlug;
    }

    // add class for the name of the custom template used (if any)
    $temp = get_page_template();
    if ( $temp != null ) {
        $path = pathinfo($temp);
        $tmp = $path['filename'] . "." . $path['extension'];
        $tn= str_replace(".php", "", $tmp);
        $classes[] = "template_".$tn;
    }

    return $classes;

}

add_filter('body_class', 'condensed_body_class');



/**
* Get the category id from a category name
*
* @param    string 
* @return   string
* @author   Keir Whitaker
*/
function get_category_id( $cat_name ){
$term = get_term_by( 'name', $cat_name, 'category' );
return $term->term_id;
}


// Removes the automatic paragraph tags from the excerpt, we leave it on for the content and have a custom field you can use to turn it off on a page by page basis --> wpautop = false
    remove_filter('the_excerpt', 'wpautop');



// This function is used to get the slug of the page
    function get_the_slug() {
        global $post;
        if ( is_single() || is_page() ) {
            return $post->post_name;
        } else {
            return "";
        }
    }


/* --------------------------------------------------
 V REGISTER SIDEBARS
 ________________________________________________________________________________________________*/



// Register wigetized sidebars, changing the default output from lists to divs

    if ( function_exists('register_sidebar') )

    register_sidebar(array(
        'id' => 'sidebar-main',
        'name' => 'Sidebar: Main',
        'description' => 'The second (secondary) sidebar.',
        'before_widget' => '<div class="%1$s widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h4 class="widgettitle">',
        'after_title' => '</h4>',
    ));
    
    // // if you want to add more just keep adding them like this:
    // register_sidebar(array(
    //     'id' => 'sidebar-footer',
    //     'name' => 'Sidebar: Footer',
    //     'description' => 'Footer sidebar.',
    //     'before_widget' => '<div class="%1$s widget %2$s">',
    //     'after_widget' => '</div>',
    //     'before_title' => '<h4 class="widgettitle">',
    //     'after_title' => '</h4>',
    // ));

/*
register_sidebar(array(
'name' => __( 'Header Widgets' ),
'id' => 'header-01-widgets',
'description' => __( 'Widgets shown in the Header Area.' ),
'before_widget' => '<aside id="%1$s" class="widget %2$s">',
'after_widget' => "</aside>",
'before_title' => '<h3>',
'after_title' => '</h3>'
));

register_sidebar(array(
'name' => __( 'Nav Widgets' ),
'id' => 'nav-widgets',
'description' => __( 'Widgets shown in the Nav area.' ),
'before_widget' => '<aside id="%1$s" class="widget %2$s">',
'after_widget' => "</aside>",
'before_title' => '<h3>',
'after_title' => '</h3>'
));

register_sidebar(array(
'name' => __( 'Content A Widgets' ),
'id' => 'content-a-widgets',
'description' => __( 'Widgets shown in the Content A area.' ),
'before_widget' => '<aside id="%1$s" class="widget %2$s">',
'after_widget' => "</aside>",
'before_title' => '<h3>',
'after_title' => '</h3>'
));

register_sidebar(array(
'name' => __( 'Content B Widgets' ),
'id' => 'content-b-widgets',
'description' => __( 'Widgets shown in the Content B area.' ),
'before_widget' => '<aside id="%1$s" class="widget %2$s">',
'after_widget' => "</aside>",
'before_title' => '<h3>',
'after_title' => '</h3>'
));

register_sidebar(array(
'name' => __( 'Content C Widgets' ),
'id' => 'content-c-widgets',
'description' => __( 'Widgets shown in the Content C area.' ),
'before_widget' => '<aside id="%1$s" class="widget %2$s">',
'after_widget' => "</aside>",
'before_title' => '<h3>',
'after_title' => '</h3>'
));

register_sidebar(array(
'name' => __( 'Footer Widgets' ),
'id' => 'footer-widgets',
'description' => __( 'Widgets shown in the Footer area.' ),
'before_widget' => '<aside id="%1$s" class="widget %2$s">',
'after_widget' => "</aside>",
'before_title' => '<h3>',
'after_title' => '</h3>'
));

register_sidebar(array(
'name' => __( 'Home Page Widgets' ),
'id' => 'home-page-widgets',
'description' => __( 'Widgets shown on the Home Page.' ),
'before_widget' => '<aside id="%1$s" class="widget %2$s">',
'after_widget' => "</aside>",
'before_title' => '<h3>',
'after_title' => '</h3>'
));


*/




/* --------------------------------------------------
 VI POST FORMATS, CUSTOM POST TYPE
 ________________________________________________________________________________________________*/




/**
 * This enables post formats. If you use this, make sure to delete any that you aren't going to use.
 */
//add_theme_support( 'post-formats', array( 'aside', 'audio', 'image', 'video', 'gallery', 'chat', 'link', 'quote', 'status' ) );





/*
Custom Post Types - include custom post types and taxonimies here e.g.

e.g. require_once( '../sReport/custom-post-types/your-custom-post-type.php' );


add_action( 'init', 'register_cpt_portfolio' );

function register_cpt_portfolio() {

    $labels = array( 
        'name' => _x( 'Portfolio Projects', 'portfolio_project' ),
        'singular_name' => _x( 'Portfolio Project', 'portfolio_project' ),
        'add_new' => _x( 'Add New', 'portfolio_project' ),
        'add_new_item' => _x( 'Add New Portfolio Project', 'portfolio_project' ),
        'edit_item' => _x( 'Edit Portfolio Project', 'portfolio_project' ),
        'new_item' => _x( 'New Portfolio Project', 'portfolio_project' ),
        'view_item' => _x( 'View Portfolio Project', 'portfolio_project' ),
        'search_items' => _x( 'Search Portfolio Projects', 'portfolio_project' ),
        'not_found' => _x( 'No portfolio projects found', 'portfolio_project' ),
        'not_found_in_trash' => _x( 'No portfolio projects found in Trash', 'portfolio_project' ),
        'parent_item_colon' => _x( 'Parent Portfolio Project:', 'portfolio_project' ),
        'menu_name' => _x( 'Portfolio Projects', 'portfolio_project' ),
    );

    $args = array( 
        'labels' => $labels,
        'hierarchical' => false,
        'description' => 'These are portfolio projects',
        'supports' => array( 'title', 'editor', 'thumbnail' ),
        'taxonomies' => array( 'post_tag' ),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 5,
        
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'has_archive' => true,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => true,
        'capability_type' => 'post'
    );

    register_post_type( 'portfolio', $args );
}

add_action("admin_init", "admin_init");
  add_action('save_post', 'save_slides');

  function admin_init(){
   add_meta_box("portfolioInfo-meta", "Portfolio Project Slides", "portfolio_meta_options", "portfolio", "side", "low");
  }

  function portfolio_meta_options() {
    global $post;
    $custom = get_post_custom($post->ID);
    $slides = $custom["slides"][0];
?>
  <label>Slides:</label><textarea rows="2" cols="30" name="slides"><?php echo $slides; ?></textarea>
<?php
  }

function save_slides() {
  global $post;
  update_post_meta($post->ID, "slides", $_POST["slides"]);
}
   
----------------------------------------------------------------------------------------------------
*/




/* --------------------------------------------------
 VII Analytics, MISC
 ________________________________________________________________________________________________*/


/*add_action('wp_footer', 'async_google_analytics');
function async_google_analytics() { ?>
    <script>
    var _gaq = [['_setAccount', 'UA-XXXXX-X'], ['_trackPageview']];
        (function(d, t) {
            var g = d.createElement(t),
                s = d.getElementsByTagName(t)[0];
            g.async = true;
            g.src = ('https:' == location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            s.parentNode.insertBefore(g, s);
        })(document, 'script');
    </script>
    */


 /*

?>