<?php
/**
* Single Portfolio Template
*/
?>

<?php get_header() ?>

<section id="main">

<div class="primary clear-fix" role="main">

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

<?php
$custom = get_post_custom($post->ID);
$display_slides = $custom["slides"][0];
?>

<article id="post-<?php the_ID() ?>" <?php post_class() ?> >

<div class="slider full">
<?php echo ( do_shortcode( get_post_meta( $post->ID , 'slides' , true ) ) ); ?>
</div>

<div class="entry-title">
<h1><?php the_title(); ?></h1>
</div><!-- .entry-title -->

<div class="entry-content">
<?php the_content(); ?>	
</div><!-- .entry-content -->


<div class="tags">
<?php the_tags( 'Client: ', ' / ' ); ?> 
</div><!-- .tags -->

<div class="entry-edit">
<?php edit_post_link( __( 'Edit' ) ) ?>
</div><!-- .entry-edit -->	
</article>

<?php endwhile; ?>

<div id="post-navigation">
<div class="nav-next"><?php previous_post_link( '%link', 'Previous Project' ) ?></div>
<div class="nav-previous"><?php next_post_link( '%link', 'Next Project' ) ?></div>
</div><!-- #post-navigation -->

</div><!-- #content -->


</section>

<?php get_footer() ?>