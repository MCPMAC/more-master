<?php
/**
* Archive Pages Template
*/
?>

<?php get_header() ?>

<section id="main">

<div class="primary clear-fix" role="main">

<div id="portfolio-list-content" class="cf">

<?php if (have_posts()) : ?>


<?php query_posts('post_type=portfolio&posts_per_page=10&caller_get_posts=1&paged='. $paged ); ?>

		<?php while (have_posts()) : the_post(); ?>


<article id="post-<?php the_ID() ?>" <?php post_class("column-2") ?> >

<div class="entry-image">
<?php echo get_the_post_thumbnail($page->ID, 'work'); ?>
</div>

<div class="entry-title">
<a href="<?php esc_url( the_permalink() ); ?>" title="Permalink to <?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a>
</div><!-- .entry-title -->

</article>

<?php endwhile; ?>

<div id="post-navigation">
<div class="nav-previous"><?php next_posts_link(__( 'Previous Projects' )) ?></div>
<div class="nav-next"><?php previous_posts_link(__( 'Next Projects' )) ?></div>
</div><!-- #post-navigation -->
		<?php else : endif; ?>

</div><!-- #content -->

</div>
</section>

<?php get_footer() ?>
