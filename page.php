<?php
/**
* Page Template
*/
?>

<?php get_header() ?>

<section id="main">

<div class="primary clear-fix" role="main">

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

<article id="post-<?php the_ID() ?>" <?php post_class() ?> >
<div class="entry-title">
<h2><?php the_title(); ?></h2>
</div><!-- .entry-title -->

<div class="entry-content">
<?php the_content(); ?>	
</div><!-- .entry-content -->

<div class="entry-edit">
<?php edit_post_link( __( 'Edit' ) ) ?>
</div><!-- .entry-edit -->
</article>

<?php endwhile; ?>

</div><!-- #content -->



</section>
<?php get_footer() ?>
